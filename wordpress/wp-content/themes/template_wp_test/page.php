<?php get_header(); ?>
    <div class="main-container">
        <div class="main wrapper clearfix">
            <div class="content">

                <?php if(have_posts()): ?>

                    <?php while(have_posts())
                    {
                        the_post();


                        the_content("MORE");

                    } ?>

                <?php else: ?>

                    Er is geen inhoud gevonden.

                <?php endif; ?>
            </div>
            <!-- Hier komt de sidebar -->


        </div> <!-- #main -->
    </div> <!-- #main-container -->

<?php get_footer(); ?>