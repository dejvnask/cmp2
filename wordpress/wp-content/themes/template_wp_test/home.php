<?php get_header(); ?>
    <div class="main-container">
        <div class="main wrapper clearfix">

            <div class="content">

                <?php
                if(have_posts())
                {
                    while(have_posts())
                    {
                        the_post();
                        ?>
                        <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <div class="box<?php echo $counter--; ?>">
                            <h4 class="date"><?php the_date(); ?></h4>
                            <?php the_content("MORE"); ?>

                        </div>
                        <?php
                    }
                }
                else
                {
                    echo 'No content available';
                }
                ?>


            </div>

            <!-- Hier komt de sidebar -->

        </div> <!-- #main -->
    </div> <!-- #main-container -->

<?php get_footer(); ?>