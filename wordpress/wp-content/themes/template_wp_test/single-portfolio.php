<?php
/**
 * Created by PhpStorm.
 * User: Dejvi Naska
 * Date: 11/05/16
 * Time: 09:15
 */
get_header(); ?>

<div class="main-container">
    <div class="main wrapper clearfix">
        <div class="content">

            <?php if(have_posts()):

                while(have_posts()) {
                    the_post();

                    echo '<article>';

                    the_title('<h1>', '</h1>');

                    the_content();
                    echo '<a href="' . get_bloginfo('url') . get_post_meta(get_the_ID(), 'projectmanager_link', true) .'">forumlink</a>';

                    echo '</article></div>';

                }?>

            <?php else:?>
                Er is geen inhoud gevonden
            <?php endif; ?>
            <!--hier komt sidebar -->


        </div> <!-- #main -->
    </div> <!-- #main-container -->

    <?php get_footer(); ?>
