<div class="footer-container">
 <footer class="mdl-mini-footer">
  <div class="mdl-mini-footer__left-section">
   <div class="mdl-logo">Deze site is niet gemaakt voor commerciële doeleinden.</div>
   <ul class="mdl-mini-footer__link-list">
    <li><a href="http://127.0.0.1:8080/cmp2eo/wordpress/contact/">Contact</a></li>
    <li><a href="http://127.0.0.1:8080/cmp2eo/wordpress/disclaimer/">Disclaimer</a></li>
    <li><a href="http://127.0.0.1:8080/cmp2eo/wordpress/privacy-policy/?loggedout=true">Privacy & Terms</a></li>
   </ul>
  </div>
  <div>
   <a href="https://twitter.com/DejviRuzi" class="icon-button twitter"><i class="icon-twitter"></i><span></span></a>
   <a href="https://www.facebook.com/DejviNaska1" class="icon-button facebook"><i class="icon-facebook"></i><span></span></a>
   <a href="https://plus.google.com/u/0/+HDbrothers95/posts" class="icon-button google-plus"><i class="icon-google-plus"></i><span></span></a>
  </div>
  <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_id' => 'footermenu' ) ); ?>
  
 </footer>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js">
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/material.min.js">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/main.js">

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
 (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
     function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='//www.google-analytics.com/analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
 ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
<!-- <script src="js/material.min.js"></script> -->

<?php wp_footer(); ?>
</body>
</html>