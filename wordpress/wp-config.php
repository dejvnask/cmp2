<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2eo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'bitnami');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z?Mok-H|o6<-FStqKC=-A2.x}`s|}`g0R:*d;z]gGeS&6P16C^T%cLe-=z*W63nd');
define('SECURE_AUTH_KEY',  'JV}QH#.Bpj_B$y3_3&p/P.6cYRRMB>($2[(CdUPd#w^X,QdZU+1$=<qt[b_z_/C_');
define('LOGGED_IN_KEY',    'pM8S~qcV,cDOb3o9@wmRyGOI_Rb$b,7[Cpwf1|&rKkw?b!RY+H~C>>T>W/WZ>>=w');
define('NONCE_KEY',        '#YAl:&Qj4GH-u(ZV=Zq}wOfk04~^f5^MoN;kwO.wPqs!^BK.8!AO>UHBN#K&2mfF');
define('AUTH_SALT',        'Y>z,(80<U[V_,u%p1T^L^JvI.>pJ%gPTHq+Ds40ITQDpc7*<5j+)|>Pgnb8F0zQI');
define('SECURE_AUTH_SALT', 'hPHlzw9Jr>zB]KYD@SC[)=8x:+i-EM:)G</o+J%=7id)0I;|y:/YO4[Z?qtn{<s^');
define('LOGGED_IN_SALT',   '/PUJ HFvnl98xMTU(gw1fKR3C=S;>$B^8`EbB f!3_,+x--e;wx3(O,UkyqaIM_=');
define('NONCE_SALT',       'h_|nXem2h0ztPvkh5L!D*fw /#:#1#Q2f@|qWP5h_K1n{B<Y`=bwK}-TznR YR&u');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
