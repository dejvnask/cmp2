<?php get_header(); ?>
    <div class="main-container">
        <div class="main wrapper clearfix">

            <div class="content">

                <?php
                if(have_posts())
                {
                    while(have_posts())
                    {
                        the_post();
                        //Print the title and the content of the current post
                        the_title();
                        the_content();

                    }
                }
                else
                {
                    echo 'No content available';
                }
                ?>

            </div>

            <!-- Hier komt de sidebar -->

        </div> <!-- #main -->
    </div> <!-- #main-container -->

<?php get_footer(); ?>