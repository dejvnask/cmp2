<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
 <title>Portfolio Dejvi Naska</title>
 <meta name="description" content="">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="apple-touch-icon" href="apple-touch-icon.png">
 <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
 <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.min.css">
 <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>img/favicon.ico" /> <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">






 <script src="/wp_content/themes/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
 <!-Hier staan je eigen links en meta tags -->
 <?php wp_head(); ?>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
 <header class="mdl-layout__header">
  <div class="mdl-layout-icon"></div>
  <div class="mdl-layout__header-row">
    <span class="mdl-layout__title">D N D</span>
    <div class="mdl-layout-spacer"></div>

    <nav class="mdl-navigation mdl-layout--large-screen-only">

       <?php
         wp_nav_menu( array ( 'theme_location' => 'logged-in', 'menu_id' => 'loggedin' ) );

         if ( is_user_logged_in() ) {

         } else {
          wp_login_form( array ( 'menu_id' => 'registermenu') );
         }
       ?>
    </nav>

  </div>
 </header>
</div>
